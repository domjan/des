#include "argumentparser.h"

#include <algorithm>
#include <iostream>
#include <string>

#include <argumentexception.h>

using namespace std;

/**
 * A Parameter can be represented by a string.
 * This string will be stored in a binary search tree (STL set).
 */
bool operator <(const Parameter &lhs, const Parameter &rhs)
{
    return lexicographical_compare(lhs.commandLine.begin(),
                                   lhs.commandLine.end(),
                                   rhs.commandLine.begin(),
                                   rhs.commandLine.end() );
}

/**
 * Comparsion operator overlading.
 */
bool operator ==(const Parameter &lhs, const Parameter &rhs)
{
    return (lhs.commandLine == rhs.commandLine);
}



/**
 * Adds an argument to the parser.
 * during the command-line parsing these parameters will be
 * the expected arguments.
 */
void ArgumentParser::addArgument(Parameter &parameter)
{
    this->parameters.insert(parameter);
}


/**
 * Parses the command line: argv, stored in a 2 dimensional array.
 */
void ArgumentParser::parse(int argc, char **argv)
{
    for(int i = 1; i<argc; i++)
    {
        std::string argword(argv[i]);
        std::set<Parameter>::iterator found;
        if( (found = parameters.find(argword)) != parameters.end())
        {
            Parameter p(*found);
            if(argc > (i + p.argCount))
            {
                for(int j = 0; j<p.argCount; j++)
                {
                    parsedArguments.insert(
                                pair<Parameter, string >(p.name, string(argv[++i])));
                }
            }
            else
            {
                cerr << "Not enough parameters for the argument: "<<argword
                     <<". Expected "<<p.argCount<<" parameters"<<endl;
                throw(ArgumentException("Error while parsing argument"));
                return;
            }
        }
        else
        {
            cerr << "Unexpected argument: "<<argword<<endl;
            throw(ArgumentException("Unexpected argument"));
        }
    }
}


/**
 * The rest of the program or the context can get the parsed arguments.
 * @throws An exception if the parameter cannot found
 */
const std::vector<std::string>
ArgumentParser::getParsedParameter(std::string parameter) const
{
    vector<string> result;
    multimap<Parameter, string >::const_iterator it
            = parsedArguments.find(parameter);
    for(it = parsedArguments.begin(); it != parsedArguments.end(); it++)
    {
        if(it->first == (Parameter)parameter)
        {
            result.push_back(it->second);
        }
    }
    if(result.empty())
    {
        cerr << "Error getting parser parameter: "<<parameter<<endl;
        throw(ArgumentException("Parameter not found"));
    }
    return result;
}
