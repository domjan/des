#-------------------------------------------------
#
# Project created by QtCreator 2013-02-24T09:43:34
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = automaton
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += .


SOURCES += main.cpp \
    automaton.cpp \
    state.cpp \
    statetest.cpp \
    minimalizetable.cpp \
    automatonalgorithm.cpp \
    automatonalgorithmcontext.cpp \
    argumentparser.cpp \
    jobcompiler.cpp

HEADERS += \
    automaton.h \
    state.h \
    statetest.h \
    minimalizetable.h \
    automatonalgorithm.h \
    statespaceminimization.h \
    automatonalgorithmcontext.h \
    argumentparser.h \
    argumentexception.h \
    jobcompiler.h

OTHER_FILES += statespaceminimization.cpp
