#ifndef AUTOMATONALGORITHMCONTEXT_H
#define AUTOMATONALGORITHMCONTEXT_H

#include <automaton.h>
#include <automatonalgorithm.h>

template <class IDType, class EventType>
class AutomatonAlgorithmContext
{
    AutomatonAlgorithm<IDType, EventType> *strategy;
public:
    AutomatonAlgorithmContext(AutomatonAlgorithm<IDType, EventType> *strategy)
        : strategy(strategy)
    { }

    Automaton<IDType, EventType> executeStrategy(Automaton<IDType, EventType> automaton)
    {
        return strategy->execute(automaton);
    }
};

#endif // AUTOMATONALGORITHMCONTEXT_H
