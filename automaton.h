#ifndef AUTOMATON_H
#define AUTOMATON_H

#include <state.h>
#include <set>
#include <memory>

template <class IDType, class EventType>
class AutomatonAlgorithm;



using namespace std;

template <class IDType, class EventType>
class Automaton
{
    typedef typename std::set< State<IDType, EventType> >::iterator StateIterator;
    typedef typename std::set< State<IDType, EventType> >::const_iterator StateConstIterator;

    StateIterator startingState, currentState;
    std::set< State<IDType, EventType> > stateSet;


    StateIterator findState(IDType id)
    {
        return stateSet.find(State<IDType, EventType>(id));
    }


    /**
     * Inserts a new state into the automaton.
     * If a state exists with that id, then it does nothing.
     *
     * @param id the unique identifier of the state
     * @returns an iterator points to the newly inserted state
     */
    StateIterator insertState(IDType id)
    {
        StateIterator state = findState(id);
        if(state == stateSet.end())
        {
            state = stateSet.insert(State<IDType, EventType>(id)).first;
        }
        return state;
    }

    /**
     * Throws an exception if the currentState is NULL.
     * It hapens, when the state is not initialized well.
     */
    void checkState(StateIterator state) const
    {
        if(state == stateSet.end())
        {
            throw std::exception("The state is NULL");
        }
    }

public:
    Automaton()
    { }

    Automaton(Automaton &other)
    {
        StateIterator it;
        for(it = other.getStateSet().begin(); it != other.getStateSet().end(); it++)
        {
            State<IDType, EventType> state = *it;
            State<IDType, EventType>::TransitionConstIterator j;
            std::vector< std::pair<EventType, IDType> > transitions;
            for(j = state.getTransitions().begin();
                j != state.getTransitions().end(); j++)
            {
                transitions.push_back(
                            std::pair<EventType, IDType>(j->first, (j->second)->getID()));
            }
            this->addState(state.getID(), transitions, state.isMarked());

            this->startingState = other.startingState;
        }
    }



    /**
     * Adds a state to the automaton if it does not exist yet.
     * Adds all the transitions to the (newly inserted) state.
     *
     * @param id is the state unique identifier
     * @param transitions a vector of event-stateID pairs
     *        contains the transitions from the inserted event.
     */
    void addState(IDType id,
                  std::vector< std::pair<EventType, IDType> > transitions,
                  bool marked = false)
    {
        StateIterator state = insertState(id);
        state->setMarked(marked);

        std::vector< std::pair<EventType, IDType> >::iterator i;
        for(i = transitions.begin(); i != transitions.end(); i++)
        {
            StateIterator toState = insertState(i->second);
            state->addTransition(i->first, toState);
        }
    }


    /**
     * Returns all the states in the automaton.
     * @return set of states
     */
    std::set< State<IDType, EventType> > &
    getStateSet() { return stateSet; }


    /**
     * Sets a state to a final state, whether marked is true or false.
     * @throw std::exception, if there is no state with id in the machine;
     */
    void setStateMarked( IDType id, bool marked )
    {
        StateIterator state = findState(id);
        checkState(state);
        state->setMarked(marked);
    }


    /**
     * Administrates the starting state of the automaton.
     * @throw std::exception, if there is no state with id in the machine;
     */
    void setStartingState(IDType id)
    {
        StateIterator state = findState(id);
        checkState(state);
        startingState = state;
    }


    StateIterator getStartingState()
    {
        return startingState;
    }


    /**
     * Tries to step from the currentState to an other state.
     *
     * @param event: tries to step with the event
     * @returns true, if a transition happened, false otherwise
     * @throw std::exception if the current state is NULL;
     */
    bool transition(EventType event)
    {
        checkState(currentState);
        StateIterator state = currentState->doTransition(event);
        if(state != NULL)
        {
            currentState = state;
            return true;
        }
        else
        {
            return false;
        }
    }


    /**
     * Returns true if the current state is a final state.
     *
     * @returns true, if the current state is marked, false otherwise
     * @throw std::exception if the current state is NULL;
     */
    bool isInAcceptingState()
    {
        checkState(currentState);
        if(currentState)
            return currentState->isMarked();
        else
            return false;
    }


    /**
     * Resets the automaton: sets the current state to
     * the starting state.
     */
    inline void reset()
    { currentState = startingState; }


    /**
     * Checks that if the machine is in deadlock or not.
     *
     * @returns true if the machine in deadlock, false otherwise
     */
    bool isInDeadLock()
    {
        checkState(currentState);
        return (currentState->getTransitions().size() == 0);
    }
};


/**
 * ostream operator overloading prints out all the states.
 */
template <class IDType, class EventType>
std::ostream& operator<< (std::ostream& out, Automaton<IDType, EventType> automaton )
{
    std::set< State<IDType, EventType> >::iterator it;
    for(it = automaton.getStateSet().begin(); it != automaton.getStateSet().end(); it++)
    {
        out << *it;
    }
    return out;
}

#endif // AUTOMATON_H
