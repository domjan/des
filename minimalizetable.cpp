#include "minimalizetable.h"

#include <iostream>
#include <sstream>
using namespace std;


/**
 * State-space minimization algorithm visualization.
 * This constructor initializes the data vector.
 * Data vectos contains all the text that should be
 * showed in cells.
 */
MinimalizeTable::MinimalizeTable(int dimensions)
    : dimensions(dimensions-1)
{
    square_width = 4;
    square_height = 3;
    for(int i = 0; i < dimensions; i++)
    {
        data.push_back(std::vector< std::vector<std::string> >());
        for(int j = 0; j < dimensions; j++)
        {
            data.at(i).push_back(std::vector<std::string>());
            for(int k = 0; k < dimensions; k++)
            {
                data.at(i).at(j).push_back("");
            }
        }
    }
}


/**
 * Puts an X to position (i, j).
 * Clears other possible pairs from that cell.
 */
void MinimalizeTable::setFlag(int i, int j)
{
    data.at(j-2).at(i-1).at(0) = "";
    data.at(j-2).at(i-1).at(1) = "  X";
    data.at(j-2).at(i-1).at(2) = "";
}


/**
 * Adds a pair to the given position, (i, j).
 * Puts the pair to the idx-th position of the cell list.
 */
void MinimalizeTable::addPair(int i, int j, int pair_first, int pair_second, int idx)
{
    stringstream ss;
    ss << "("<<pair_first<<", "<<pair_second<<")";
    data.at(j-2).at(i-1).at(idx) = ss.str();
    //cout<<"\n";
    repaint();
}


/**
 * Pads a string to given length. Puts (str.length-size) spaces
 * after the text.
 *
 * @param str the input string. String length should be less than "size".
 * @param size pads the "str" string to "size" length.
 */
std::string pad(std::string str, unsigned int size)
{
    int len = str.length();
    for(unsigned int i = 0; i<(size-len); i++)
    {
        str.append(" ");
    }
    return str;
}


/**
 * (Re)paints the table to the console.
 */
void MinimalizeTable::repaint()
{
    cout << "\n   "<<1<<endl;
    for(int i = 0; i < dimensions; i++)
    {
        cout << endl;

        //square top
        for(int j = 0; j <= i; j++)
        {
            cout << "+";
            for(int k = 0; k<square_width; k++)
            {
                cout <<"--";
            }
            if(i == j)
                cout <<"+"<<endl;
        }

        //square sides
        for(int l = 0; l<square_height; l++)
        {
            for(int j = 0; j <= i; j++)
            {
                //left wall and the content
                cout << "|";
                cout << pad(data.at(i).at(j).at(l), square_width*2);

                //wall or number
                if(i == j)
                {
                    if(l == square_height/2)
                    {
                        cout <<"|  "<<i+2<<endl;
                    }
                    else
                    {
                        cout <<"|"<<endl;
                    }
                }
            }
        }

        //square bottom
        for(int j = 0; j <= i; j++)
        {
            cout << "+";
            for(int k = 0; k<square_width; k++)
            {
                cout <<"--";
            }
            if(i == j)
                cout <<"+";
        }
    }
}
