#ifndef STATESPACEMINIMIZATION_H
#define STATESPACEMINIMIZATION_H

#include <automatonalgorithm.h>
#include <minimalizetable.h>

template <class IDType, class EventType>
class StateSpaceMinimization : public AutomatonAlgorithm<IDType, EventType>
{
private:
    typedef std::pair< State<IDType, EventType>, State<IDType, EventType> > StatePair;
    typedef std::map< StatePair, std::vector< StatePair > > StatePairsList;
    typedef typename std::set< State<IDType, EventType> >::iterator StateIterator;
    MinimalizeTable *table;

public:
    StateSpaceMinimization()
        : table(NULL)
    { }

    void setMinimizationTable(MinimalizeTable *mt)
    {
        table = mt;
    }

    Automaton<IDType, EventType> execute(Automaton<IDType, EventType> automaton);
};


//template class definition in cpp file
#include <statespaceminimization.cpp>

#endif // STATESPACEMINIMIZATION_H
