#include "statespaceminimization.h"

#include <iostream>

using namespace std;

template <class IDType, class EventType>
Automaton<IDType, EventType> StateSpaceMinimization<IDType, EventType>::execute(
        Automaton<IDType, EventType> automaton)
{
    std::set< State<IDType, EventType> > stateSet = automaton.getStateSet();
    std::set< State<IDType, EventType> >::iterator it, it2, it3;
    std::set< StatePair > pairList;
    StatePairsList lists;

    //sort out different active event set state pairs
    //  and marked/non marked pairs
    for(it = stateSet.begin(); it != stateSet.end(); it++)
    {
        //combination without repetition
        //state pair ids will be ordered ascending
        it3 = it;
        for(it2 = ++it3; it2 != stateSet.end(); it2++)
        {
            //cout << "Check pair: "<<*it<<", "<<*it2<<", "<<(it->activeEventSet() != it2->activeEventSet())<<endl;
            if(it->isMarked() != it2->isMarked()
                    || it->activeEventSet() != it2->activeEventSet())
            {
                table->setFlag(it->getID(), it2->getID());
                pairList.insert(StatePair (StatePair(*it, *it2)));
            }
        }
    }

    //run through all the combinations again
    for(it = stateSet.begin(); it != stateSet.end(); it++)
    {
        it3 = it;
        for(it2 = ++it3; it2 != stateSet.end(); it2++)
        {
            StateIterator s1, s2;

            //check onlz the not flagged pairs
            if(pairList.find(StatePair(*it, *it2)) == pairList.end()) //not flagged pairs
            {
                std::set<EventType> events = it->activeEventSet();
                std::set<EventType>::iterator eventIterator;
                bool someAlreadyFlagged = false;

                //check all the transitions from the pair
                for(eventIterator = events.begin(); eventIterator != events.end(); eventIterator++)
                {
                    s1 = it->doTransition(*eventIterator);
                    s2 = it2->doTransition(*eventIterator);

                    //pairs can be compared if they are ordered
                    if(*s2 < *s1)
                        std::swap(s1, s2);

                    if(pairList.find(StatePair(*s1, *s2)) != pairList.end()) //if flagged
                    {
                        someAlreadyFlagged = true;
                        break;
                    }
                }

                //if at least one of the transition leads to unmergeable(flagged) pairs
                if(someAlreadyFlagged)
                {
                    //then flag (x, y)
                    pairList.insert(StatePair (StatePair(*it, *it2)));
                    table->setFlag(it->getID(), it2->getID());
                    table->repaint();

                    //flag unflagged (w, z) pairs in (x, y)'s list
                    StatePairsList::iterator lit = lists.find(StatePair(*it, *it2));
                    if(lit != lists.end())
                    {
                        std::vector< StatePair > list = (*lit).second;
                        std::vector< StatePair >::iterator it6;
                        for(it6 = list.begin(); it6 != list.end(); it6++)
                        {
                            pairList.insert(*it6);
                            table->setFlag(it6->first.getID(), it6->second.getID());
                            table->repaint();
                        }
                    }
                }
                else //none of them flagged
                {
                    //add this pair to every rtansition pairs list
                    for(eventIterator = events.begin(); eventIterator != events.end(); eventIterator++)
                    {
                        s1 = it->doTransition(*eventIterator);
                        s2 = it2->doTransition(*eventIterator);

                        if(*s1 == *s2)
                            continue;

                        //pairs can be compared if they are ordered
                        if(*s2 < *s1)
                            std::swap(s1, s2);

                        //if there is already a list in the matrix, then
                        // append to that
                        StatePairsList::iterator lit = lists.find(StatePair(*s1, *s2));
                        std::vector< StatePair > list;
                        if(lit != lists.end())
                        {
                            list = (*lit).second;
                            lists.erase(StatePair(*s1, *s2));
                        }

                        //visualization
                        table->addPair(s1->getID(), s2->getID(),
                                       it->getID(), it2->getID(), list.size());

                        //push new pair into the list
                        list.push_back(StatePair(*it, *it2));

                        //insert it to the matrix
                        lists.insert(std::pair< StatePair, std::vector< StatePair > >(
                                         StatePair(*s1, *s2), list));
                    }
                }
            }
        }
    }

    //print out the result
    cout << "\nThe pairs can be merged: "<<endl;
    for(it = stateSet.begin(); it != stateSet.end(); it++)
    {
        it3 = it;
        for(it2 = ++it3; it2 != stateSet.end(); it2++)
        {
            if(pairList.find(StatePair(*it, *it2)) == pairList.end())
            {
                cout << "(" << it->getID() << ", " << it2->getID() << ")"<<endl;
            }
        }
    }

    return Automaton<IDType, EventType>();
}
