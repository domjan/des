#ifndef ARGUMENTPARSER_H
#define ARGUMENTPARSER_H

#include <map>
#include <vector>
#include <set>

class Parameter
{
public:
    Parameter(std::string commandLine,
              std::string name,
              unsigned int parameterArgCount)
        : commandLine(commandLine),
          name(name),
          argCount(parameterArgCount)
    { }

    /**
     * Automatic conversion from string
     */
    Parameter(std::string &commandLine)
        : commandLine(commandLine)
    { }

    std::string commandLine;
    std::string name;
    int argCount;
};


/**
 * Class ArgumentParser parses the command line arguments,
 * and stores the parsed datas into a multimap.
 */
class ArgumentParser
{
private:
    std::multimap<Parameter, std::string > parsedArguments;
    std::set<Parameter> parameters;

public:
    ArgumentParser() { }
    void addArgument(Parameter &parameter);
    void parse(int argc, char **argv);

    const std::vector<std::string>
    getParsedParameter(std::string parameter) const;

};

#endif // ARGUMENTPARSER_H
