#include <QtCore/QCoreApplication>

#include <argumentparser.h>
#include <iostream>
#include <argumentexception.h>
#include <jobcompiler.h>

using namespace std;

int main(int argc, char *argv[])
{
    ArgumentParser args;
    args.addArgument(Parameter("-a", "automaton", 1));
    args.addArgument(Parameter("-t", "task", 1));
    try
    {
        args.parse(argc, argv);
        JobCompiler job(args);
        job.doJob();
    }
    catch(ArgumentException e)
    {
        cerr << "Argument exception caught: "<<e.what()<<endl;
        cerr << "Basic usage examples: \n\n"
             <<"Doing a test algorithm on an automaton: ./automaton -t test\n";
        return 1;
    }
    return 0;
}
