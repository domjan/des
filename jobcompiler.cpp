#include "jobcompiler.h"
#include <statetest.h>
#include <vector>
#include <argumentexception.h>
#include <iostream>

using namespace std;

JobCompiler::JobCompiler(ArgumentParser args)
    : tasksToDo(0)
{
    vector<string> tasks = args.getParsedParameter("task");
    if(tasks.size() == 0)
    {
        cerr << "There is no tasks "<<endl;
        throw(ArgumentException("Should be at least one task among arguments"));
    }

    for(vector<string>::iterator i = tasks.begin(); i<tasks.end(); i++)
    {
        string task = *i;
        if(task.compare("test") == 0)
            tasksToDo |= TASK_TEST;
    }
}


void JobCompiler::doJob()
{
    if(tasksToDo == 0)
    {
        cerr << "There is no recognised tasks to do "<<endl;
    }

    if( tasksToDo & TASK_TEST )
    {
        StateTest s;
    }
}
