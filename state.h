#ifndef STATE_H
#define STATE_H

#include <vector>
#include <map>
#include <set>
#include <ostream>

template <class IDType, class EventType>
class State
{
public:
    //locate the state inside the autmaton
    typedef typename std::set< State<IDType, EventType> >::iterator StateIterator;

    typedef typename std::map< EventType, StateIterator > TransitionMap;
    typedef typename TransitionMap::iterator TransitionIterator;
    typedef typename TransitionMap::const_iterator TransitionConstIterator;
    typedef typename std::pair< EventType, StateIterator > Transition;


    State(IDType id)
        : id(id), marked(false)
    { }

    void addTransition(EventType event, StateIterator state)
    {
        this->transitions.insert(Transition(event, state));
    }

    const TransitionMap &getTransitions() const
    { return transitions; }

    IDType getID() const { return id; }
    bool isMarked() const { return marked; }

    void setMarked(bool marked)
    { this->marked = marked; }


    /**
     * Tries to do a transition with an event.
     * @returns An iterator points to the transition function result.
     *      If there is no such event then it returns NULL.
     */
    StateIterator doTransition(EventType event)
    {
        TransitionIterator it;
        it = transitions.find(event);
        if(it != transitions.end())
        {
            return (StateIterator)it->second;
        }
        else
        {
            return NULL;
        }
    }


    /**
     * Returns the state active event set, where
     * the transition f(x, a) is feasible.
     * @returns set of events
     */
    std::set<EventType> activeEventSet()
    {
        std::set<EventType> events;
        TransitionIterator it;
        for(it = transitions.begin(); it != transitions.end(); it++)
        {
            events.insert(it->first);
        }
        return events;
    }

private:
    IDType id;
    bool marked;
    TransitionMap transitions;
};



// Operator overloadings....


template <class IDType, class EventType>
bool operator== (const State<IDType, EventType> &first, const State<IDType, EventType> &second )
{
    return (first.getID() == second.getID());
}


template <class IDType, class EventType>
bool operator!= (const State<IDType, EventType> &first, const State<IDType, EventType> &second )
{
    return !(first.getID() == second.getID());
}


template <class IDType, class EventType>
bool operator< (const State<IDType, EventType> &first, const State<IDType, EventType> &second )
{
    return (first.getID() < second.getID());
}


template <class IDType, class EventType>
bool operator> (const State<IDType, EventType> &first, const State<IDType, EventType> &second )
{
    return (second.getID() < first.getID());
}


template <class IDType, class EventType>
std::ostream& operator<< (std::ostream& out, State<IDType, EventType> state )
{
    out << "State { "
        <<"id = "<<state.getID()<<"; "
       <<"marked = "<<state.isMarked()<<";";

    //print each transition
    State<IDType, EventType>::TransitionConstIterator i, checkLastIterator;
    for(i = state.getTransitions().begin();
        i != state.getTransitions().end(); i++)
    {
        out << " Transition [ ";
        out << "event = "<<i->first<<" ";
        out << "state id = "<<i->second->getID()<<" ";
        out << "marked = "<<i->second->isMarked()<<" ";

        checkLastIterator = i;
        if(++checkLastIterator == state.getTransitions().end() )
        {
            out << "]";
        }
    }
    out<<"}"<<std::endl;
    return out;
}

#endif // STATE_H
