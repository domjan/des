#ifndef MINIMALIZETABLE_H
#define MINIMALIZETABLE_H

#include <string>
#include <vector>

class MinimalizeTable
{
    int dimensions;
    int square_width;
    int square_height;

    std::vector< std::vector< std::vector<std::string> > > data;

public:
    MinimalizeTable(int dimensions);
    void repaint();
    void setFlag(int i, int j);
    void addPair(int i, int j, int pair_first, int pair_second, int idx);
};

#endif // MINIMALIZETABLE_H
