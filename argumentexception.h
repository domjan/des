#ifndef ARGUMENTEXCEPTION_H
#define ARGUMENTEXCEPTION_H

#include <exception>


class ArgumentException : public std::exception
{
public:
    ArgumentException(const char *message)
        : std::exception(message)
    { }
};

#endif // ARGUMENTEXCEPTION_H
