#include "statetest.h"

#include <state.h>
#include <automaton.h>
#include <iostream>
#include <istream>
#include <sstream>

#include <statespaceminimization.h>
#include <minimalizetable.h>
#include <automatonalgorithmcontext.h>

using namespace std;


Automaton<int, char> buildAutomaton(std::string str)
{
    Automaton<int, char> automaton;
    std::istringstream f(str);
    std::string trfunclist;
    while(std::getline(f, trfunclist, ';')) //every transition functionlist: "1->a:4,b:5;"
    {
        std::istringstream f2(trfunclist);
        std::string from, temp, with, to, with_to;
        std::getline(f2, from, '-');
        std::getline(f2, temp, '>');

        //get prefixes from state character
        //the prefix can be . or > indicate the marked and the starting state
        bool startingState = false, markedState = false;
        while(!isdigit(from[0]))
        {
            char prefix = from[0];
            from = from.substr(1, from.length()-1);
            if(prefix == '.')
            {
                markedState = true;
            }
            else if(prefix == '>')
            {
                startingState = true;
            }
        }

        std::vector< std::pair<char, int> > transitions_vector;
        while(std::getline(f2, with_to, ','))
        {
            std::istringstream f3(with_to);
            std::getline(f3, with, ':');
            std::getline(f3, to, ':');

            //cout << "Transition: "<<from<<" "<<with<<" "<<to<<endl;
            transitions_vector.push_back(std::pair<char, int>(with.at(0), atoi(to.c_str())));
        }
        automaton.addState(atoi(from.c_str()), transitions_vector, markedState);
        if(startingState)
        {
            automaton.setStartingState(atoi(from.c_str()));
        }
    }
    return automaton;
}


StateTest::StateTest()
{
    Automaton<int, char> automaton = buildAutomaton("1->a:4,b:5;" \
                                                           ".2->a:1;" \
                                                           "3->a:2,b:2;" \
                                                           ".4->a:7,b:8;" \
                                                           ">5->a:6;" \
                                                           ".6->a:3,b:2;" \
                                                           "7->a:8,b:2;" \
                                                           ".8->a:9;" \
                                                           "9->a:6,b:5;"
                );


    /*
    std::vector< std::pair<char, int> > trs;
    trs.clear();
    trs.push_back(std::pair<char, int>('a', 4));
    trs.push_back(std::pair<char, int>('b', 5));
    a.addState(1, trs);
*/

    /*
    automaton.setStateMarked(2, true);
    automaton.setStateMarked(4, true);
    automaton.setStateMarked(6, true);
    automaton.setStateMarked(8, true);
    automaton.setStartingState(5);
*/
    //cout << *automaton.getStateSet().begin();

    cout << automaton;


    StateSpaceMinimization<int, char> minimization;
    MinimalizeTable mt(9);
    minimization.setMinimizationTable(&mt);

    AutomatonAlgorithmContext<int, char> ctx(&minimization);
    ctx.executeStrategy(automaton);

    mt.repaint();

}
