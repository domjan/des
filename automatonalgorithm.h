#ifndef AUTOMATONALGORITHM_H
#define AUTOMATONALGORITHM_H

#include <automaton.h>

//template function cannot be virtual so
//so I created a template class

template <class IDType, class EventType>
class AutomatonAlgorithm
{
public:
    AutomatonAlgorithm()
    { }

    virtual Automaton<IDType, EventType> execute(Automaton<IDType, EventType> automaton) = 0;
};

#endif // AUTOMATONALGORITHM_H
