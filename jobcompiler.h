#ifndef JOBCOMPILER_H
#define JOBCOMPILER_H

#include <argumentparser.h>
#include <argumentexception.h>
#include <automaton.h>

class JobCompiler
{
private:
    enum Task
    {
        TASK_TEST = 0x1
    };

    int tasksToDo;
    std::vector< Automaton<int, char> > operands;

public:
    JobCompiler(ArgumentParser args);
    void doJob();
};

#endif // JOBCOMPILER_H
